require_relative './test_helper'
require_relative '../lib/location'

class LocationTest < Minitest::Test

  def test_distance
    # the distance is ~ 463km based on
    # http://www.distancefromto.net/distance-from-london-gb-to-dublin-ie
    london = Location.new 51.5074, 0.1278
    dublin = Location.new 53.3498, 6.2603
    assert_equal 463, london.distance(dublin).to_i
  end

end
