require_relative './test_helper'
require_relative '../lib/array'

class ArrayTest < Minitest::Test

  def test_nested_flatten
    assert_equal [1, 2, 3, 4], [[1, 2, [3]], 4].deep_flatten
    assert_equal [1, 2, 3, 4], [1, 2, 3, 4].deep_flatten
  end

end
