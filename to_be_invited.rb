require 'json'
require_relative 'lib/location'
require_relative 'lib/customer'

office = Location.new 53.3393, -6.2576841

customers = File.readlines('data/customers.data').map do |record_in_text|
  Customer.from_json JSON.parse(record_in_text)
end.sort_by(&:id)

close_by_customers = customers.select do |customer|
  customer.location.distance(office) <= 100 # km
end

close_by_customers.each do |customer|
  puts "#{customer.id}\t#{customer.name}"
end
