# What's included?

* Nested array flatter
* Location based filtering of users

# Installation

Use `bundler` to install the dependencies:

```bash
bundle install
```

# Run

To see the list of users that should be invited, do this:

```bash
ruby to_be_invited.rb
```

It reads the list of users from `data/customers.data` and prints out a sorted
list of users who are within 100km from Dublin office.

# Tests

Use `rake` to run the tests:

```bash
rake test
```
