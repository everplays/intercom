class Customer < Struct.new(:id, :name, :location)

  class << self

    def from_json record
      new record['user_id'],
        record['name'],
        Location.new(record['latitude'].to_f, record['longitude'].to_f)
    end

  end

end
