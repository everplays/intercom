class Location < Struct.new(:latitude, :longitude)

  RadiusOfEarth = 6371 # in kilo meters

  # the calculation is based on http://www.movable-type.co.uk/scripts/latlong.html
  def distance other_location
    # φ1 = teta_1
    # φ2 = teta_2
    # Δφ = latitude_delta
    # Δλ = longitude_delta
		teta_1 = to_radians(latitude)
		teta_2 = to_radians(other_location.latitude)
    latitude_delta = to_radians(other_location.latitude - latitude)
    longitude_delta = to_radians(other_location.longitude - longitude)

    a = power2(Math.sin(latitude_delta / 2)) + Math.cos(teta_1) * Math.cos(teta_2) * power2(Math.sin(longitude_delta / 2))
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))

    RadiusOfEarth * c
  end

private

  def power2 number
    number ** 2
  end

  def to_radians degrees
    degrees * Math::PI / 180
  end

end
