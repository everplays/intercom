class Array

  def deep_flatten
    # XXX: using flat_map would make this much simpler
    inject([]) do |accumulator, item|
      if item.is_a? Array
        item.deep_flatten.each do |nested_item|
          accumulator << nested_item
        end
      else
        accumulator << item
      end
      accumulator
    end
  end

end
